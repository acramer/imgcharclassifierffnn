# imgCharClassifierFFNN

## The goal of this project is to properly implement a feed-forward network using PyTorch.

Input: a 10x10 image as a features space.
Output: a char or NULL classification.

#### Dependancies and software versions used:
* Python    3.7
* PyTorch   1.0.1
* NumPy     1.16.2
* SciPy     1.2.1
